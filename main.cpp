#ifndef __PROGTEST__

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <deque>
#include <map>
#include <set>
#include <list>
#include <algorithm>

#if defined ( __cplusplus ) && __cplusplus > 199711L /* C++ 11 */

#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <functional>

#endif /* C++ 11 */

using namespace std;
#endif /* __PROGTEST__ */

class NoRouteException {
};

template<typename _T, typename _E>
class Node {
private:


    bool _visited= false;
    vector<pair<_T,_E>> _routes;

public:
    Node()=default;


    void addRoute(const _T & adjecentTile,const _E & route){
        _routes.push_back(pair<_T,_E>(adjecentTile,route));


    }
    _T getNode(int i)const {
        return _routes[i].first;
    }
    _E getRoute(int i) {
        return _routes[i].second;
    }
    int routesSize()const {
        return _routes.size();
    }
//    bool operator<(const Node &node) const {
//        return _node < node._node;
//    }
//
//    bool operator==(const Node &node) const {
//        return _node == node._node;
//    }
    void setVisited(bool visited){
        _visited= visited;
    }
    bool wasVisited(){
        return _visited;
    }



};

template<typename _T, typename _E>
class CRoute {
private:
    map<_T,Node<_T, _E>> _tree;
public:
    CRoute() = default;
    ~CRoute()=default;



    CRoute &Add( const _T & tile1, const  _T& tile2, const _E& route) {
        Node<_T, _E> node1;
        Node<_T, _E> node2;
        auto position1 = _tree.find(tile1);
        auto position2 = _tree.find(tile2);
        if (position1 == _tree.end()) {
            _tree.insert(pair<_T,Node<_T, _E>>(tile1,node1));
            position1 = _tree.find(tile1);
        }
        if (position2 == _tree.end()) {
            _tree.insert(pair<_T,Node<_T, _E>>(tile2,node2));
            position2 = _tree.find(tile2);
        }
        position1->second.addRoute(tile2,route);

        position2->second.addRoute(tile1,route);
        return *this;
    }

    list<_T> Find(const _T &start, const _T &finish, function<bool(_E)> x) {
        bool found=false;
        list<_T> path;
        path.push_front(finish);

        if(start==finish){
            return path;
        }
        bool atEnd=false;
        auto startPosition = _tree.find(start);
        auto endPosition = _tree.find(finish);
        if(startPosition==_tree.end()||endPosition==_tree.end()){
            throw NoRouteException();
        }
        auto itr=_tree.begin();
        auto tmpItr=_tree.begin();


        for (unsigned int i =0;i<_tree.size();i++){
            itr->second.setVisited(false);
            itr++;
        }

        itr=_tree.find(start);
        queue<_T> bfsQueue;
        while(!atEnd){
            //get queue from iterator
            for(int i=0;i<itr->second.routesSize();i++){
                //find node that I want to add to queue
                tmpItr=_tree.find(itr->second.getNode(i));
                //see if it already is in queue

                if(tmpItr->second.wasVisited()||!x(itr->second.getRoute(i))){
                    continue;
                }
                tmpItr->second.setVisited(true);
                bfsQueue.push(itr->second.getNode(i));
            }
            ///..........................................///
            //todo contains path to end of path

            for(int i=0;i<itr->second.routesSize();i++){
                ////todo wierd
                tmpItr=_tree.find(itr->second.getNode(i));
                if(path.front()==tmpItr->first){
                    path.push_front(itr->first);
                    if(path.front()==start){
                        atEnd=true;
                        found=true;
                        break;
                    }else{
                        itr=_tree.begin();
                        while(!bfsQueue.empty()){
                            bfsQueue.pop();
                        }
                        for (unsigned int j =0;j<_tree.size();j++){
                            itr->second.setVisited(false);
                            itr++;
                        }
                        itr=_tree.find(start);
                        found=true;



                    }
                }
            }
            if(found){
                found =false;
                continue;
            }
            ///next in queue and delete from queue
            if(bfsQueue.empty()){
                throw NoRouteException();

            }
            itr=_tree.find(bfsQueue.front());
            bfsQueue.pop();

        }

        return path;
    }

    list<_T> Find(const _T &start, const _T &finish) {
        return Find(start,finish,[](const _E &x) { return true; });
    }
};

#ifndef __PROGTEST__
//
//////=================================================================================================
//class CTrain {
//public:
//    CTrain(const string &company,
//           int speed)
//            : m_Company(company),
//              m_Speed(speed) {
//    }
//
//    ////---------------------------------------------------------------------------------------------
//    string m_Company;
//    int m_Speed;
//};
//
//////=================================================================================================
//class TrainFilterCompany {
//public:
//    TrainFilterCompany(const set<string> &companies)
//            : m_Companies(companies) {
//    }
//
//    ////---------------------------------------------------------------------------------------------
//    bool operator()(const CTrain &train) const {
//        return m_Companies.find(train.m_Company) != m_Companies.end();
//    }
//    ////---------------------------------------------------------------------------------------------
//private:
//    set<string> m_Companies;
//};
//
//////=================================================================================================
//class TrainFilterSpeed {
//public:
//    TrainFilterSpeed(int min,
//                     int max)
//            : m_Min(min),
//              m_Max(max) {
//    }
//
//    ////---------------------------------------------------------------------------------------------
//    bool operator()(const CTrain &train) const {
//        return train.m_Speed >= m_Min && train.m_Speed <= m_Max;
//    }
//    ////---------------------------------------------------------------------------------------------
//private:
//    int m_Min;
//    int m_Max;
//};
//
//////=================================================================================================
//bool NurSchnellzug(const CTrain &zug) {
//    return (zug.m_Company == "OBB" || zug.m_Company == "DB") && zug.m_Speed > 100;
//}
//
//////=================================================================================================
//static string toText(const list<string> &l) {
//    ostringstream oss;
//
//    auto it = l.cbegin();
//    oss << *it;
//    for (++it; it != l.cend(); ++it)
//        oss << " > " << *it;
//    return oss.str();
//}
//
//////=================================================================================================
//int main(void) {
//    CRoute<string, CTrain> lines;
//
//    lines.Add("Berlin", "Prague", CTrain("DB", 120))
//            .Add("Berlin", "Prague", CTrain("CD", 80))
//            .Add("Berlin", "Dresden", CTrain("DB", 160))
//            .Add("Dresden", "Munchen", CTrain("DB", 160))
//            .Add("Munchen", "Prague", CTrain("CD", 90))
//            .Add("Munchen", "Linz", CTrain("DB", 200))
//            .Add("Munchen", "Linz", CTrain("OBB", 90))
//            .Add("Linz", "Prague", CTrain("CD", 50))
//            .Add("Prague", "Wien", CTrain("CD", 100))
//            .Add("Linz", "Wien", CTrain("OBB", 160))
//            .Add("Paris", "Marseille", CTrain("SNCF", 300))
//            .Add("Paris", "Dresden", CTrain("SNCF", 250));
//
//    list<string> r1 = lines.Find("Berlin", "Linz");
//    assert (toText(r1) == "Berlin > Prague > Linz");
//
//    list<string> r2 = lines.Find("Linz", "Berlin");
//    assert (toText(r2) == "Linz > Prague > Berlin");
//
//    list<string> r3 = lines.Find("Wien", "Berlin");
//    assert (toText(r3) == "Wien > Prague > Berlin");
//
//    list<string> r4 = lines.Find("Wien", "Berlin", NurSchnellzug);
//    assert (toText(r4) == "Wien > Linz > Munchen > Dresden > Berlin");
//
//    list<string> r5 = lines.Find("Wien", "Munchen", TrainFilterCompany(set<string>{"CD", "DB"}));
//    assert (toText(r5) == "Wien > Prague > Munchen");
//
//    list<string> r6 = lines.Find("Wien", "Munchen", TrainFilterSpeed(120, 200));
//    assert (toText(r6) == "Wien > Linz > Munchen");
//
//    list<string> r7 = lines.Find("Wien", "Munchen", [](const CTrain &x) { return x.m_Company == "CD"; });
//    assert (toText(r7) == "Wien > Prague > Munchen");
//
//    list<string> r8 = lines.Find("Munchen", "Munchen");
//    assert (toText(r8) == "Munchen");
//
//    list<string> r9 = lines.Find("Marseille", "Prague");
//    assert (toText(r9) == "Marseille > Paris > Dresden > Berlin > Prague"
//            || toText(r9) == "Marseille > Paris > Dresden > Munchen > Prague");
//
//    try {
//        list<string> r10 = lines.Find("Marseille", "Prague", NurSchnellzug);
//        assert ("Marseille > Prague connection does not exist!!" == NULL);
//    }
//    catch (const NoRouteException &e) {
//    }
//
//    list<string> r11 = lines.Find("Salzburg", "Salzburg");
//    assert (toText(r11) == "Salzburg");
//
//    list<string> r12 = lines.Find("Salzburg", "Salzburg", [](const CTrain &x) { return x.m_Company == "SNCF"; });
//    assert (toText(r12) == "Salzburg");
//
//    try {
//        list<string> r13 = lines.Find("London", "Oxford");
//        assert ("London > Oxford connection does not exist!!" == NULL);
//    }
//    catch (const NoRouteException &e) {
//    }
//    return 0;
//}

#endif  /* __PROGTEST__ */
